const FoxyClient = require('../FoxyClient').FoxyClient
const GITLAB = require('../GitLab/GitLab').GitLab
const usersFeatures = require('../GitLab/features/users');

/*
Set GITLAB_COM_TOKEN:
sudo pico ~/.bash_profile
export GITLAB_COM_TOKEN="⚙️🔮💣"
*/
let gitLabCli = new FoxyClient({
  kind: GITLAB({baseUri:"https://gitlab.com/api/v4", token: process.env.GITLAB_COM_TOKEN}) 
}, usersFeatures)

gitLabCli.fetchUserByHandle('k33g')
  .then(user => {
    console.log("😀 user:", user);
  })
  .catch(error => {
    console.log("😡 error", error)
  });

gitLabCli.fetchUserById(`841428`)
  .then(user => {
    console.log("😀 ⚠️ user:", user);
  })
  .catch(error => {
    console.log("😡 error", error)
  });  

gitLabCli.fetchUser({handle: 'k33g'}).then(user => {
  console.log("👋 user:", user);
})

gitLabCli.fetchProjectsOfUser({handle: 'k33g', perPage:3}).then(projects => {
  projects.forEach(project => {
    console.log("🚧" ,project.id, project.name, project.path_with_namespace)
  })
})



