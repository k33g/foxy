const FoxyClient = require('../FoxyClient').FoxyClient
const GITLAB = require('../GitLab/GitLab').GitLab
const usersFeatures = require('../GitLab/features/users');
const groupsFeatures = require('../GitLab/features/groups');


/*
Set GITLAB_COM_TOKEN:
sudo pico ~/.bash_profile
export GITLAB_COM_TOKEN="⚙️🔮💣"
*/
let gitLabCli = new FoxyClient({
  kind: GITLAB({baseUri:"https://gitlab.com/api/v4", token: process.env.GITLAB_COM_TOKEN}) 
}, usersFeatures, groupsFeatures)


gitLabCli.fetchGroups({perPage:5}).then(data => {
  data.forEach(item => {
    console.log(item.id, item.name)
  })
})

gitLabCli.fetchGroup({name:"wey-yu"}).then(data => {
  console.log(data)
})









