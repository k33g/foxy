const fetch = require('node-fetch');

class FoxyClient {
  constructor({kind}, ...features) {
    return Object.assign(this, kind, ...features)
  }

  callAPI({method, path, data}) { //TODO handle error resonse.status etc ...
    //let _response = {};
    return fetch(this.baseUri + path, {
      method: method,
      headers: this.headers,
      body: data!==null ? JSON.stringify(data) : null
    })
    .then(response => response.json())
    .then(jsonData => jsonData)

  }

  getData({path}) {
    return this.callAPI({method:'GET', path, data:null});
  }

  deleteData({path}) {
    return this.callAPI({method:'DELETE', path, data:null});
  }

  postData({path, data}) {
    return this.callAPI({method:'POST', path, data});
  }

  putData({path, data}) {
    return this.callAPI({method:'PUT', path, data});
  }

}
module.exports = {
  FoxyClient: FoxyClient
}