
function fetchUserByHandle(handle) { // get user data
  return this.getData({path:`/users?username=${handle}`})
    .then(response => response[0])
    .catch(error => error);
}

function fetchUserById(id) { // get user data
  return this.getData({path:`/users/${id}`})
    .then(response => response)
    .catch(error => error);
}

function fetchUser({handle}) {
  return this.getData({path:`/users?username=${handle}`})
    .then(response => {
      return this.getData({path:`/users/${response[0].id}`})
      
    })
}

function fetchProjectsOfUser({handle, perPage}) {
  return this.getData({path:`/users?username=${handle}`})
    .then(response => {
      return this.getData({path:`/users/${response[0].id}/projects?per_page=${perPage==undefined?20:perPage}`})
    })
}

module.exports = {
  fetchUserByHandle: fetchUserByHandle,
  fetchUserById: fetchUserById,
  fetchUser: fetchUser,
  fetchProjectsOfUser: fetchProjectsOfUser
};

