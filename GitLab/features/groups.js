

function fetchGroups({perPage}) {
  return this.getData({path:`/groups?per_page=${perPage==undefined?20:perPage}`})
}

function fetchGroup({name}) {
  return this.getData({path:`/groups?search=${name}`})
    .then(response => response[0])
    .catch(error => error);
}

module.exports = {
  fetchGroups: fetchGroups,
  fetchGroup: fetchGroup
};

