function GitLab({baseUri, token}) {
  return {
    baseUri: baseUri,
    token: token,
    headers: {
      "Content-Type": "application/json",
      "Private-Token": token
    }
  }
}

module.exports = {
  GitLab: GitLab
}
